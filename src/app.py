

from flask import Flask, render_template, redirect, url_for

from src.common.database import Database

from src.models.domain.domain import Domain


app = Flask(__name__)
app.config.from_object('config')
app.secret_key = "123"
app.config['UPLOAD_FOLDER']='static/assests'



@app.before_first_request
def init_db():
    Database.initialize()



@app.route('/')
def home():
    return redirect(url_for("profiles.topcourse"))

from src.models.users.view import user_blueprint
from src.models.course_profile.view import Profile_blueprint
from src.models.course.view import course_blueprint
from src.models.domain.view import Domain_blueprint

app.register_blueprint(Profile_blueprint,url_prefix="/profiles")
app.register_blueprint(Domain_blueprint, url_prefix="/domains")
app.register_blueprint(user_blueprint, url_prefix="/users")
app.register_blueprint(course_blueprint, url_prefix="/Courses")
