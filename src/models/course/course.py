import uuid


import src.models.course.constants as CourseConstant
from src.common.database import Database
import elasticsearch


from src.models.domain.domain import Domain
import src.models.users.error as UserError



class Course(object):
    def __init__(self,title,teacher,description,period,link,img,domain,_id=None):
        self.title = title
        self.teacher = teacher
        self.description = description
        self.period = period
        self.link=link
        self.img=img
        self.domain=domain
        self._id = uuid.uuid4().hex if _id is None else _id
        #dom = Course.get_by_domain_name(self.domain)
        #self.content = dom.content
        #self.pic = dom.img

    def save(self):
        Database.insert(CourseConstant.COLLECTION,self.json())

    def json(self):
        return {
            "title":self.title,
            "teacher":self.teacher,
            "description":self.description,
            "period":self.period,
            "link":self.link,
            "img":self.img,
            "domain":self.domain,
            "_id":self._id
        }
    @classmethod
    def find_Domain(cls,domain):

        return [cls(**elem) for elem in Database.find(CourseConstant.COLLECTION,{"domain":domain})]
    @classmethod
    def find_Teachercourse(cls,name):

        return [cls(**elem) for elem in Database.find(CourseConstant.COLLECTION,{"teacher":name})]

    @classmethod
    def get_by_domain_name(cls,domain):
        data=Domain.get_details(domain)
        return data

    def addcourse(name,content,img,teacher,period,link,dom):
        m = Course(name,teacher,content,period,link,img,dom)
        m.save()
        return True
    @classmethod
    def get_by_id(cls,course_id):
        return cls(**Database.find_one(CourseConstant.COLLECTION,{"_id":course_id}))


    def editcourse(name,content,img,teacher,period,link,dom,cour):

        j = Course(name,teacher,content,period,link,img,dom,_id=cour)
        j.update()
        return True
    @classmethod
    def search(cls,name):

         return [cls(**elem) for elem in Database.search(CourseConstant.COLLECTION,name)]



    def update(self):
        Database.update(CourseConstant.COLLECTION,{"_id": self._id},self.json())

    def delete(cour):
        Database.delete_one("course", {"_id": cour})
        return True


    @classmethod
    def getfull(cls):
        return [cls(**elem) for elem in Database.find(CourseConstant.COLLECTION, {})]
