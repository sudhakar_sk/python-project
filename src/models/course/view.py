import os


import src.models.users.decorators as user_decorator
from flask import Blueprint, render_template, request, url_for, session
from werkzeug.utils import redirect, secure_filename

from src.app import app
from src.models.course.course import Course
from src.models.course_profile.mycourse import Mycourse
from src.models.domain.domain import Domain
from src.models.users.teacher import Teacher
from src.models.users.test import Test

course_blueprint = Blueprint('Courses', __name__)


@course_blueprint.route("/")
@user_decorator.requires_login
def index():
    return render_template("course/course_home.html", domain=Domain.all())


@course_blueprint.route("/onlinetest/<string:course_id>/<string:title>", methods=['POST','GET'])
def onlinetest(course_id,title):
    course = Test.get_by_test(course_id,title)
    correct=0

    if request.method=='POST':
        for c in course:
            try:
                Answer = request.form[c._id]
            except:
                error = "fill all the question"
                return render_template("users/online test.html", domain=Domain.all(), course_id=course_id,
                                       course=course, title=title,error = error)
            v=c.Answer
            if Answer.strip() == v.strip():
                correct=correct+1

        return render_template("course/result.html", domain = Domain.all(),correct=correct)



    return render_template("users/online test.html",domain=Domain.all(),course_id=course_id,course = course,title = title)



@course_blueprint.route("/course_page/<string:course_id>")
def coursepage(course_id):
    course = Course.get_by_id(course_id)

    find_test = Test.Find_distinct("title",course_id)
    print(find_test)

    return render_template("player.html",link=course.link,domain=Domain.all(),course_id=course_id,find_test=find_test)

@course_blueprint.route("/viewuser/<string:course_id>")
def viewuser(course_id):
    user = Mycourse.get_course_by_id(course_id)
    return render_template("coursetest.html",user = user)



@course_blueprint.route("/<string:domain>")
def course(domain):
    data = Course.find_Domain(domain)
    try:
        if session['email'] is not None:
            pro = Mycourse.get_course_by_email(session['email'])
        else:
            pro=None
    except KeyError:
        pro = None
    dom = Domain.get_details(domain)
    return render_template('course/ml.html', data=data, dom=dom, course=pro, domain=Domain.all())

@course_blueprint.route("/coursetest/<string:course_id>",methods =['GET','POST'])
def coursetest(course_id):
    course = Course.get_by_id(course_id)

    find_test = Test.Find_distinct("title", course_id)
    return render_template("coursetest.html", find_test=find_test)

@course_blueprint.route("/<string:domain>/<string:cor_id>")
def select(domain,cor_id):
    data = Course.find_Domain(domain)
    try:
        if session['email'] is not None:
            pro = Mycourse.get_course_by_email(session['email'])
        else:
            pro=None
    except KeyError:
        pro = None
    cor = Course.get_by_id(cor_id)


    return render_template('course/click.html', data=data, course=pro, domain=Domain.all(),cor = cor)



@course_blueprint.route("/addcourse", methods=['GET', 'POST'])

def addcourse():
    error=None
    if request.method == 'POST':
        name = request.form['name']
        content = request.form['Description']
        res=Teacher.findone(session['email'])
        if res==None:
            error="you are not a authorized teacher"
            return render_template("course/Add_course.html", domain=Domain.all(), error=error)
        teacher = res.name

        period = request.form['period']
        link = request.form['link']
        dom = request.form['Domain']
        try:
            file = request.files['file']
        except KeyError:
            error="choose a file"
            return render_template("course/Add_course.html", domain=Domain.all(), error=error)

        if(len(name)>0 and len(content)>0 and len(teacher)>0 and len(period)>0 and len(link)>0 and len(dom)>0):

            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                img = filename
                if Course.addcourse(name, content, img, teacher, period, link, dom):
                    return redirect(url_for("users.teacherprofile"))
        error="fill all the details correctly"
    return render_template("course/Add_course.html", domain=Domain.all(),error=error)


@course_blueprint.route("/editcourse/<string:cour>", methods=['GET', 'POST'])

def editCourse(cour):
    error = None
    if request.method == 'POST':
        name = request.form['name']
        content = request.form['Description']
        res = Teacher.findone(session['email'])
        if res == None:
            error = "you are not a authorized teacher"
            return render_template("course/edit_course.html", domain=Domain.all(), error=error)
        teacher = res.name


        period = request.form['period']
        link = request.form['link']
        dom = request.form['Domain']
        try:
            file = request.files['file']
        except KeyError:
            error="choose a file"
            c = Course.get_by_id(cour)
            return render_template("course/edit_course.html", domain=Domain.all(),cour=c, error=error)
        if (len(name) > 0 and len(content) > 0 and len(period) > 0 and len(
                link) > 0 and len(dom) > 0):

            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                img = filename
                if Course.editcourse(name, content, img, teacher, period, link, dom, cour):
                    return redirect(url_for("Courses.course",domain = dom))
        error="fill all the fields"
    c = Course.get_by_id(cour)
    return render_template("course/edit_Course.html", domain=Domain.all(), cour=c,error=error)


@course_blueprint.route("/deletecourse/<string:cour>/<string:domain>")
@user_decorator.requires_admin_permission
def deleteCourse(cour,domain):
    if Course.delete(cour):
        return redirect(url_for("Courses.course", domain=domain))
@course_blueprint.route("/search",methods=['POST'])
def searching():
    if request.method=='POST':
        name=request.form['search']
        key = Course.search(name)
        try:
            if session['email'] is not None:
                pro = Mycourse.get_course_by_email(session['email'])
            else:
                pro=None
        except KeyError:
            pro = None
        return render_template("course/search.html",key=key,course=pro,domain=Domain.all())

