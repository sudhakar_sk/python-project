import uuid
import src.models.domain.constants as DomainConstant
from src.common.database import Database
class Domain:
    def __init__(self,name,content,img,_id=None):
        self.name=name
        self.content=content
        self.img=img

        self._id = uuid.uuid4().hex if _id is None else _id

    def save(self):
        Database.insert(DomainConstant.COLLECTION, self.json())

    def json(self):
        return {
            "name":self.name,
            "content":self.content,
            "img":self.img,
            "_id":self._id
        }
    @classmethod
    def get_details(cls,domain):
        return cls(**Database.find_one(DomainConstant.COLLECTION,{"name":domain}))
    @classmethod
    def get_by_id(cls,domain_id):
        return cls(**Database.find_one(DomainConstant.COLLECTION,{"_id":domain_id}))


    @classmethod
    def all(cls):
        return [cls(**elem) for elem in Database.find('domain', {})]
    def addDomain(name,content,img):
        user = Database.find_one(DomainConstant.COLLECTION, {"name": name})
        if user is None:
            s = Domain(name,content,img)
            s.save()
            return True
        return False

    def editDomain(name,content,img,domain_id):
        j = Domain(name,content,img,domain_id)
        j.update()
        return True

    def update(self):
        Database.update(DomainConstant.COLLECTION, {"_id": self._id}, self.json())
