import os

from flask import Blueprint, request, url_for, render_template
from werkzeug.utils import redirect, secure_filename
import src.models.users.decorators as user_decorator
from src.app import app
from src.models.domain.domain import Domain

Domain_blueprint=Blueprint('Domains',__name__)

@Domain_blueprint.route("/addDomain",methods =['GET','POST'])
@user_decorator.requires_admin_permission
def addDomain():
    error=None
    if request.method=='POST':
        name=request.form['name']
        content=request.form['content']

        try:
            file = request.files['file']
        except KeyError:
            error = "choose a file"
            return render_template("Domain/addDomain.html", domain=Domain.all(), error=error)

        if(len(name)>0 and len(content)>0 and file):
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
                img = filename
            if Domain.addDomain(name,content,img):
                return redirect(url_for("users.homepage"))
        error="fill all the details"
    return render_template("Domain/addDomain.html",domain=Domain.all(),error=error)

@Domain_blueprint.route("/editDomain/<string:domain_id>",methods =['GET','POST'])
@user_decorator.requires_admin_permission
def editDomain(domain_id):
    error = None
    if request.method=='POST':
        name=request.form['name']
        content=request.form['content']
        try:
            file = request.files['file']
        except KeyError:
            error="choose a file"
            c = Domain.get_by_id(domain_id)
            return render_template("Domain/edit_domain.html", domain=Domain.all(), do=c, error=error)

        if (len(name) > 0 and len(content) > 0 and file):
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                img = filename

            if Domain.editDomain(name,content,img,domain_id):
                return redirect(url_for("Courses.course",domain = Domain.get_by_id(domain_id).name))
        error = "fill all the details"
    c=Domain.get_by_id(domain_id)
    return render_template("Domain/edit_domain.html",domain=Domain.all(),do = c,error=error)
