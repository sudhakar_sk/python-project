import uuid

from src.common.database import Database
from src.common.utils import Utils


class Teacher(object):

    def __init__(self,email,password,name,domain,_id = None):
        self.email = email
        self.password = password
        self.name=name
        self.domain = domain
        self._id = uuid.uuid4().hex if _id is None else _id
    def register(email,password,name,domain):
        if len(name)<=0 :
            return "enter the username"
        if len(password)<=0:
            return "enter the password"
        if len(email) <= 0:
            return "enter the email"
        if len(domain) <= 0:
            return "enter the domain"

        user_data = Database.find_one('teacher', {"email": email})
        if user_data is None:
            if Utils.email_is_valid(email):
                data =Teacher(email,Utils.hash_password(password),name,domain)
                data.Save_to_mongo()
                return True
            return "Invalid email"
        return "Error:User Already Exists"

    @classmethod
    def findone(cls,email):
        return cls(**Database.find_one('teacher',{"email":email}))
    def Save_to_mongo(self):
       Database.insert('teacher', self.json())

    def json(self):
       return {
        'email': self.email,
        'password': self.password,
        'name':self.name,
        'domain':self.domain,
        '_id': self._id
        }

    def login(email,password):
        if len(email)<=0 :
            return "enter the email"
        if len(password) <= 0:
            return "enter the password"
        user = Database.find_one('teacher', {"email": email})
        if user is None:
            return "please enter the correct credentiality user doesn't exists"

        else:
            if not Utils.check_hashed_password(password,user['password']):
                return "check ur password"
            return True
