import uuid

from src.common.database import Database


class Test:
    def __init__(self,title,Question,Answer,choice1,choice2,choice3,course_id,_id=None):
        self.title=title
        self.Question =Question
        self.Answer = Answer
        self.choice1 = choice1
        self.choice2 = choice2
        self.choice3 =choice3
        self.course_id =course_id
        self._id = uuid.uuid4().hex if _id is None else _id
    @classmethod
    def findone(cls,id):
        return cls(**Database.find_one('test',{"_id":id}))
    def Save_to_mongo(self):
       Database.insert('test', self.json())

    def json(self):
        return{
        "title":self.title,
        "Answer":self.Answer,
        "Question":self.Question,
        "choice1":self.choice1,
        "choice2":self.choice2,
        "choice3":self.choice3,
        "course_id":self.course_id,
        "_id":self._id
    }

    @classmethod
    def Find_by_courseid(cls,course_id):

        return [cls(**elem) for elem in Database.find("test",{"course_id":course_id})]

    @classmethod
    def Find_distinct(cls, data,course_id):
        return Database.distinct("test", data,{'course_id':course_id})

    @classmethod
    def get_by_test(cls, course_id, Title):
        return [cls(**elem) for elem in Database.find("test", {"course_id": course_id ,"title": Title})]

    def update(self):
        return Database.update("test",{"_id":self._id},self.json())

