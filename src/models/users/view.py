from profile import Profile

from flask import Blueprint, request, session, url_for, render_template
from werkzeug.utils import redirect

from src.models.course.course import Course
from src.models.course_profile.mycourse import Mycourse
from src.models.domain.domain import Domain
from src.models.users.teacher import Teacher
from src.models.users.test import Test
from src.models.users.user import User
import src.models.users.decorators as user_decorator
user_blueprint = Blueprint('users',__name__)
@user_blueprint.route("/login",methods =['GET','POST'])
def login_user():
    error=None
    if request.method=='POST':
        email=request.form['email']
        password=request.form['password']

        if User.login(email,password)==True:
            session['email']=email


            return redirect(url_for("Courses.index"))
        error=User.login(email,password)
    return render_template("users/login.html",domain=Domain.all(),error=error)

@user_blueprint.route("/loginteacher",methods =['GET','POST'])
def login_teacher():
    error=None
    if request.method=='POST':
        email=request.form['email']
        password=request.form['password']

        if Teacher.login(email,password)==True:
            session['email']=email
            session['name'] = email
            return redirect(url_for("users.teacherprofile"))
        error=Teacher.login(email,password)
    return render_template("users/teacher.html",domain=Domain.all(),error=error)
@user_blueprint.route("/selfprofile",methods =['GET','POST'])
def selfprofile():
    res = Teacher.findone(session['email'])
    return render_template("selfprofile.html",res=res)

@user_blueprint.route("/teachernav",methods =['GET','POST'])
def teachernav():
    return render_template("nav.html")

@user_blueprint.route("/teacherprofile",methods =['GET','POST'])
def teacherprofile():
    return render_template("teacherprofile.html")

@user_blueprint.route("/teacherprofile2",methods =['GET','POST'])
def teacherprofile2():
    error = None
    res = Teacher.findone(session['email'])
    collect = Course.find_Teachercourse(res.name)
    tname = Teacher.findone(session['email'])
    user = Mycourse.get_course_by_teacher(tname.name)
    print(user)


    return render_template("users/teacherprofile.html",data=collect,user=user)


@user_blueprint.route("/test/<string:cour_id>",methods =['GET','POST'])
def test(cour_id):
    error=None
    if request.method=='POST':
        Title = request.form['name']
        for i in range(5):

            Question=request.form['Q'+str(i)]
            Answer = request.form['A'+str(i)]
            c1 =request.form['Q'+str(i)+'C1']
            c2 = request.form['Q'+str(i)+'C2']
            c3 = request.form['Q'+str(i)+'C3']
            if len(Question)>0 and len(c1)>0 and len(c2)>0 and len(c3)>0 and len(Title)>0 and len(Answer)>0:
                if (Answer.strip() == c1.strip() or Answer.strip() == c2.strip() or Answer.strip() == c3.strip()):
                    T = Test(Title,Question,Answer,c1,c2,c3,cour_id)
                    T.Save_to_mongo()
                    return render_template("course/test.html", error=error, cour_id=cour_id)
                error="answer did not exists in choice"
                return render_template("course/test.html", error=error, cour_id=cour_id)

            error="fill all the fields"
    return render_template("course/test.html",error=error,cour_id=cour_id)
@user_blueprint.route("/listoftest/<string:cour_id>")
def listoftest(cour_id):
    error = None
    #course = Course.get_by_id(cour_id)

    find_test = Test.Find_distinct("title", cour_id)
    print(find_test)
    return render_template("list.html",error=error,find_test=find_test,cour_id=cour_id)


@user_blueprint.route("/edittest/<string:cour_id>/<string:title>",methods =['GET','POST'])
def edittest(cour_id,title):
    error=None
    b = Test.get_by_test(cour_id,title)
    if request.method=='POST':
        Title = request.form['name']
        for i in b:
            print(i._id)
            Question=request.form['Q'+i._id]
            Answer = request.form['A'+i._id]
            print(Answer)
            c1 =request.form['Q'+i._id+'C1']
            c2 = request.form['Q'+i._id+'C2']
            c3 = request.form['Q'+i._id+'C3']
            id =  request.form['id'+i._id]
            if len(Question)>0 and len(c1)>0 and len(c2)>0 and len(c3)>0 and len(Title)>0 and len(Answer)>0:
                if (Answer.strip() == c1.strip() or Answer.strip() == c2.strip() or Answer.strip() == c3.strip()):
                    T = Test(Title,Question,Answer,c1,c2,c3,cour_id,id)
                    T.update()
                else:
                    error="answer did not exists in choice"
                    return render_template("edittest.html", error=error, cour_id=cour_id,b=b,title=title)
            else:
                error="fill all the fields"
                return render_template("edittest.html", error=error, cour_id=cour_id, b=b, title=title)
        return redirect(url_for('users.listoftest',cour_id=cour_id))
    return render_template("edittest.html",error=error,cour_id=cour_id, b=b,title=title)




@user_blueprint.route('/register',methods = ['GET','POST'])
def register_user():
    error=None

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']

        if User.register(email,password,name)==True:
            session['email']=email
            return redirect(url_for("Courses.index"))
        error = User.register(email,password,name)


    return render_template("users/register.html",domain=Domain.all(),error=error)


@user_blueprint.route('/register_teacher',methods = ['GET','POST'])
def register_teacher():
    error=None

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        domain = request.form['Domain']
        if Teacher.register(email,password,name,domain)==True:

            return redirect(url_for(".homepage"))
        error = User.register(email,password,name)


    return render_template("users/regteacher.html",domain=Domain.all(),error=error)

@user_blueprint.route('/logout')
def logout_user():
    session['email']= None
    session['name']=None
    return redirect(url_for(".homepage"))
@user_blueprint.route('/home')
def homepage():
    return redirect(url_for("profiles.topcourse"))


@user_blueprint.route("/join/<string:course_id>", methods=['GET', 'POST'])
@user_decorator.requires_login
def needverify(course_id):
    error=None
    if request.method == 'POST':
        email = session['email']
        password = request.form['password']
        if User.verify(email,password)==True:
            if Mycourse.join(email, course_id):
                return redirect(url_for('profiles.mycourses'))
        error=User.verify(email,password)
    return render_template("users/register_course.html",domain=Domain.all(),course_id=course_id,error=error)





