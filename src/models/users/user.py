import uuid

from flask import session

from src.common.utils import Utils
import src.models.users.constants as UserConstants
from src.common.database import Database
import src.models.users.error as UserError

class User(object):

    def __init__(self,email,password,name,_id = None):
        self.email = email
        self.password = password
        self.name=name
        self._id = uuid.uuid4().hex if _id is None else _id
    def login(email,password):
        if len(email)<=0 :
            return "enter the email"
        if len(password) <= 0:
            return "enter the password"
        user = Database.find_one(UserConstants.COLLECTION, {"email": email})
        if user is None:
            return "please enter the correct credentiality user doesn't exists"

        else:
            if not Utils.check_hashed_password(password,user['password']):
                return "check ur password"
            return True

    def register(email,password,name):
        if len(name)<=0 :
            return "enter the username"
        if len(password)<=0:
            return "enter the password"
        user_data = Database.find_one('user', {"email": email})
        if user_data is None:
            if Utils.email_is_valid(email):
                data =User(email,Utils.hash_password(password),name)
                data.Save_to_mongo()
                return True
            return "Invalid email"
        return "Error:User Already Exists"

    def verify(email,password):
        user_data = Database.find_one('user', {"email": email})
        if not Utils.check_hashed_password(password, user_data['password']):
            return "check ur password"
        return True


    def Save_to_mongo(self):
       Database.insert('user', self.json())


    def json(self):
       return {
        'email': self.email,
        'password': self.password,
        'name':self.name,
        '_id': self._id
        }