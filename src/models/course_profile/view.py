import csv

import os


from flask import Blueprint, session, render_template, request
from werkzeug.utils import secure_filename

from src.app import app


from src.models.course.course import Course
from src.models.course_profile.mycourse import Mycourse
from src.models.domain.domain import Domain
import sys
import pandas as pd

Profile_blueprint=Blueprint('profiles',__name__)
@Profile_blueprint.route('/')
def mycourses():
    data = Mycourse.get_course_by_email(session['email'])
    return render_template("course/profile.html",data = data,domain=Domain.all())
@Profile_blueprint.route('/topcourse')
def topcourse():
    top = Course.getfull()

    top6=[]
    count=0
    for x in range(len(top)-1,-1,-1):
        count=count+1
        if(count <= 6):
            top6.append(top[x])
        else:
            break
    return render_template("home.html", top=top6, domain=Domain.all())

@Profile_blueprint.route('/addfile',methods=["POST","GET"])
def addfile():
    error=None
    if request.method=='POST':
        file = request.files['file']
        if file:

            results = []
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            with open("static/assests/"+filename, 'r') as f:
                reader = csv.DictReader(f)

                for row in reader:
                    results = pd.json.dumps(row)
                    print(results)



        else:
            error="fault"

    return render_template("summa.html",domain=Domain.all(),error=error)
