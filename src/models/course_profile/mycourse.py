import uuid

import requests

from src.common.database import Database
from src.models.course.course import Course
import src.models.course_profile.constants as profileconstant
import src.models.users.error as UserError


class Mycourse(object):
    def __init__(self,email,course_id,title=None,teacher=None,link=None,img=None,_id=None):
        self.email=email
        self.course_id=course_id
        self._id = uuid.uuid4().hex if _id is None else _id
        detail = Course.get_by_id(self.course_id)
        self.title = detail.title if title is None else title
        self.teacher=detail.teacher if teacher is None else teacher
        self.link = detail.link if link is None else link
        self.img = detail.img if img is None else img


    def save(self):
        Database.insert('profile', self.json())

    def json(self):
        return {
            "email":self.email,
            "course_id":self.course_id,
            "_id":self._id,
            "title":self.title,
            "teacher":self.teacher,
            "link":self.link,
            "img":self.img
        }

    def join(email,course_id):
        data=Mycourse(email, course_id)
        data.send()
        data.save()

        return True
    @classmethod
    def get_course_by_email(cls,email):
        return [cls(**elem) for elem in Database.find('profile',{"email":email})]

    @classmethod
    def get_course_by_teacher(cls, name):
        return [cls(**elem) for elem in Database.find('profile', {"teacher": name})]

    def send(self):
        try:
            return requests.post(
            profileconstant.URL,
            auth=("api", profileconstant.API_KEY),
            data={
                "from": profileconstant.FROM,
                "to": self.email,
                "subject": " U have successfully registered for online course in {}".format(self.title),
                "text": "ComputerEra, welcomes u ,The link for course is {}".format(self.link)
            })
        except(Exception):
            return Exception
    @classmethod
    def get_course_by_id(cls,cour):
        return [cls(**elem) for elem in Database.find('profile', {"course_id": cour})]

    @classmethod
    def getall(cls):
        return [cls(**elem) for elem in Database.find('profile', {})]

    def delete(cour):
        if Mycourse.get_course_by_id(cour) is None:
            Database.delete_one("course",{"_id":cour})
            return True
        raise UserError.UserExists("cant delete the file the user is using it")